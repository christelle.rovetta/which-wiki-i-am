import requests
from typing import Final, Any

import wikipediaapi
from tqdm import tqdm


class WikipediaTools:
    # Used to find valid pages in wikipedia
    _PERSON_KEYWORDS: Final[set[str]] = set(["born", "died"])
    _MIN_SUMMARY_SIZE: Final[int] = 300

    def __init__(self, project_name: str = "WhoAmI", language: str = "en") -> None:
        self.wiki_api = wikipediaapi.Wikipedia(
            project_name, language, extract_format=wikipediaapi.ExtractFormat.WIKI
        )

    @staticmethod
    def random_page_id() -> str | None:
        """
        Returns a random Wikipedia page ID.

        Returns:
            str | None: A random Wikipedia page ID, or None if an error occurs.
        """
        random_page_url = "https://en.wikipedia.org/wiki/Special:Random"
        try:
            response = requests.get(random_page_url)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            return None
        else:
            page_id = response.url.split("/")[-1]
            return page_id

    def random_person_page(
        self, max_attempts: int = 20, verbose: bool = True
    ) -> str | None:
        """Find a random Wikipedia page ID that refer to a person.

        Args:
            max_attempts (int, optional): maximum number of attempts to fin a page. Defaults to 100.

        Returns:
            str | None: wikipedia page id
        """

        def _is_valid(page_id: str) -> bool:
            if "(" in page_id:
                return False

            page = self.wiki_api.page(page_id)
            summary = page.summary

            if len(summary) < WikipediaTools._MIN_SUMMARY_SIZE:
                return False
            for keyword in WikipediaTools._PERSON_KEYWORDS:
                if keyword in summary[:150]:
                    return True

        # Find a random Wikipedia page ID that has a valid summary.
        # - Display progress bar
        if verbose:
            print("I'm thinking of someone ...")
            gen = tqdm(range(max_attempts))
        else:
            gen = range(max_attempts)
        # - Try to find a person
        for _ in gen:
            page_id = self.random_page_id()
            if page_id and _is_valid(page_id=page_id):
                return page_id
        return None

    def page_info(self, page_id: str) -> dict[str:Any] | None:
        """Get infos relative to a Wikipedia page.

        Args:
            page_id (str): Wikipedia page ID.

        Returns:
            dict[str: Any] | None: Wikipedia page summary, or None if an error occurs.
        """
        try:
            page = self.wiki_api.page(page_id)
            return {"summary": page.summary, "text": page.text}
        except wikipediaapi.exceptions.PageError as e:
            # TODO Logger
            print(e)
            return None
