import torch
from langchain_text_splitters import RecursiveCharacterTextSplitter
from langchain.embeddings import HuggingFaceEmbeddings
from langchain_community.vectorstores import Chroma as LChroma


class TextRetrieval:
    def __init__(
        self, k: int = 3, chunk_size: int = 4000, chunk_overlap: int = 200
    ) -> None:
        self.chunk_size = chunk_size
        self.chunk_overlap = chunk_overlap
        self.k = k
        self.text = None

    def new_text(self, text: str):
        self.text = text
        self.do_retrieval = len(self.text) > self.chunk_size * self.k

        if self.do_retrieval:
            # - Split data into chunks
            text_splitter = RecursiveCharacterTextSplitter(
                chunk_size=self.chunk_size,
                chunk_overlap=self.chunk_overlap,
                length_function=len,
                add_start_index=True,
                separators=[
                    "\n\n",
                    "\n",
                    " ",
                    ".",
                    ",",
                    "",
                ],
            )
            chunks = text_splitter.create_documents([self.text])

            device = "cuda" if torch.cuda.is_available() else "cpu"
            self.embedding_function = HuggingFaceEmbeddings(
                model_kwargs={"device": device}
            )
            vector_db = LChroma.from_documents(
                chunks, embedding=self.embedding_function
            )
            self.retriever = vector_db.as_retriever(search_kwargs={"k": self.k})

    def get_context_as_txt(self, query: str) -> str:
        if self.text is None:
            return ""

        if not self.do_retrieval:
            return self.text

        documents = self.retriever.get_relevant_documents(query)
        txt = [doc.page_content for doc in documents]
        return " ".join(txt)
