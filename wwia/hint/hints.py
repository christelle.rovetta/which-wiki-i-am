import random

from .catalog.factory import BaseHint, hint_factory


class Hints(BaseHint):
    def __init__(self, secret: str, hints_config: dict) -> None:
        super().__init__(secret=secret)
        self.hints = [
            hint_factory(hint_id, secret=secret, **hints_config[hint_id])
            for hint_id in hints_config
        ]
        random.shuffle(self.hints)
        self.count = 0

        if len(self.hints) == 0:
            self.given = True

    def _give(self) -> str:
        # Select the first hint in the list
        hint = self.hints.pop(0)
        hint_to_show = hint.give()
        self.count += 1

        # Re use hint if it was not given
        if not hint.given:
            self.hints.append(hint)

        if len(self.hints) == 0:
            self.given = True

        return f"**Hint {self.count}:** {hint_to_show}"
