import httpx
import ollama
from ollama import Client

from .base_hint import BaseHint


class HintTalkLike(BaseHint):
    def __init__(
        self,
        secret: str,
        ollama_model_id: str = "llama3",
        num_sentences: int = 5,
    ) -> None:
        super().__init__(secret=secret)
        self.ollama_model_id = ollama_model_id

        # Test ollama connection
        try:
            ollama.pull(ollama_model_id)  # Load the model if needed
        except httpx.ConnectError:
            self.given = True
            num_sentences = 0

        # Generate sentences
        self.about_me = [self.generate() for _ in range(num_sentences)]

    @property
    def prompt(self) -> str:
        return f"""You are '{self.secret}'. 
        Said something about your life.
        You you must never mention your surname. 
        You must express yourself in your mother tongue. 
        Express yourself in one sentence.
        Do not translate your sentence."""

    def generate(self) -> str:
        """Generate new hint

        Returns:
            str: hint
        """

        client = Client(host="http://localhost:11434")
        response = client.chat(
            model=self.ollama_model_id,
            options={"temperature": 0.7},
            messages=[
                {
                    "role": "system",
                    "content": self.prompt,
                },
            ],
        )
        self.given = False
        return response.get("message", {}).get("content", "...")

    def _give(self) -> str:
        my_hint = self.about_me.pop()
        if len(self.about_me) == 0:
            self.given = True

        return my_hint
