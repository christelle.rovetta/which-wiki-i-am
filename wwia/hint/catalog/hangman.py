import random
from typing import Final

from .base_hint import BaseHint


class HintHungman(BaseHint):
    MISSING_CHAR: Final[str] = "."
    HINT_SHOW: Final[str] = "My name is: `.`"

    def __init__(self, secret: str, difficulty: int = 3) -> None:
        super().__init__(secret=secret)
        self.difficulty = difficulty

        self.hint = [
            (" " if letter == " " else HintHungman.MISSING_CHAR) for letter in secret
        ]
        self.to_give = [i for i, letter in enumerate(secret) if letter != " "]
        random.shuffle(self.to_give)

        self.difficulty = max(2, self.difficulty)
        self.update_ratio = min(1, len(secret) / self.difficulty)

    def _update(self):
        for _ in range(self.update_ratio + 1):
            if len(self.to_give) > 0:
                i = self.to_give.pop()
                self.hint[i] = self.secret[i]

        self.given = len(self.to_give) == 0

    def _give(self) -> str:
        hint_to_show = "".join(self.hint)
        self._update()

        return HintHungman.HINT_SHOW.replace(HintHungman.MISSING_CHAR, hint_to_show)
