from abc import ABC, abstractmethod


class BaseHint(ABC):
    def __init__(self, secret: str) -> None:
        self.given = False
        self.secret = secret

    def __str__(self) -> str:
        return f"{str(self.__class__).split(".")[-1]}"

    @abstractmethod
    def _give(self) -> str:
        raise NotImplementedError

    def give(self) -> str:
        if not self.given:
            return self._give()
        return "..."
