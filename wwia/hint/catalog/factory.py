from typing import Final, Callable
from enum import StrEnum

from .base_hint import BaseHint
from .hangman import HintHungman
from .talk_like import HintTalkLike


class HintId(StrEnum):
    HANGMAN = "hangman"
    TALK_LIKE = "talk_like"


HINT_CATALOG: Final[dict[str, Callable]] = {
    HintId.HANGMAN.value: HintHungman,
    HintId.TALK_LIKE.value: HintTalkLike,
}


def hint_factory(hint_id: str, secret: str, **hint_config) -> BaseHint:
    return HINT_CATALOG[hint_id](secret=secret, **hint_config)
