from typing import Any, Final

import gradio as gr

from wwia.utils.wikipedia import WikipediaTools
from wwia.utils.text_retrieval import TextRetrieval
from wwia.pipeline_llm.round_openai import PipelineRoundOpenAI
from wwia.hint.hints import Hints
from wwia.hint.catalog.factory import HintId


class GameWWIA:
    END_GAME: Final[str] = "END"

    def __init__(
        self, underrepresented: bool = False, hint_frequency: int | None = 4, k: int = 3
    ):
        # Wikipedia random search
        self.underrepresented = underrepresented
        self.wiki = WikipediaTools(project_name="GameWWIA", language="en")
        # Model
        self.txt_retrieval = TextRetrieval(k=k)
        self.pipeline_round = PipelineRoundOpenAI()
        # Hint parameters
        self.hint_frequency = hint_frequency

        # Game init
        self.init: bool = False
        self.page_id: str | None = None
        self.person: str = ""
        self.text: str = ""
        self.count_round: int = 0
        self.hint = None

    def __str__(self) -> str:
        return f"Current page: {self.page_id}"

    def _get_page_id(self) -> str:
        return self.wiki.random_person_page(max_attempts=50)

    @staticmethod
    def _pageId_to_name(page_id: str) -> str:
        name = page_id.replace("_", " ")
        return name

    def _get_text(self, page_id: str) -> str:
        page_info = self.wiki.page_info(page_id=page_id)
        return page_info["text"]

    def _init_round(self) -> None:
        """Init a new round with a new person to guess"""

        # Do not init
        if self.init:
            return

        # First round
        self.init = True
        self.count_round = 0

        # Search on wikipedia
        self.page_id = None
        while self.page_id is None:
            self.page_id = self._get_page_id()
        self.person = GameWWIA._pageId_to_name(page_id=self.page_id)

        # Reset text relative to the person
        self.text = self._get_text(page_id=self.page_id)
        self.txt_retrieval.new_text(text=self.text)

        # Reset Hints
        self.hint = Hints(
            secret=self.person,
            hints_config={
                HintId.HANGMAN.value: {"difficulty": 3},
                HintId.TALK_LIKE.value: {"num_sentences": 2},
            },
        )

    def _infer_round(self, user_query: str) -> dict[str, Any]:
        """Use LMM  to answer to the user query

        Args:
            user_query (str): user query

        Returns:
            dict[str, Any]: message
        """

        # RAG
        txt_context = self.txt_retrieval.get_context_as_txt(query=user_query)
        try:
            response = self.pipeline_round.invoke(
                person=self.person, text=txt_context, user_query=user_query
            )
        except Exception as e:
            # TODO: log with the error
            print(f"Error: {e}")
            response = (
                {"answer": "Sorry, I have a memory lapse ..."}
                if self.count_round < 20  # Try less than 20
                else {"win": True}
            )
        finally:
            return response

    def round(
        self,
        user_query: str,
    ) -> tuple[bool, str]:
        # Infer using `round` LLM pipeline
        response = self._infer_round(user_query=user_query)
        self.count_round += 1

        # - User supposition
        if "win" in response:
            if response["win"]:
                return (
                    True,
                    f"Yes I'm `{self.person}`! You win in {self.count_round} rounds!",
                )
            return False, f"Sorry, I'm not `{response["supposition"]}`"

        # - User question
        if "answer" in response:
            return False, f"{response["answer"]}"

        # - Non-compliant user query
        return False, "Sorry, I can't answer..."

    def run_terminal(self) -> None:
        """Run the game in a terminal"""
        # Init the game
        self._init_round()

        # Rounds
        # - First round
        user_query = input(f"\n {self.count_round}. - Enter your question:")
        end_game, system_response = self.round(user_query=user_query)
        print(">>", system_response)
        # - Rounds
        while not end_game:
            user_query = input(f"\n {self.count_round}. - Enter your question:")
            if user_query == "42":
                system_response = f"I'm `{self.person}` 🦾"
            elif user_query == self.person:
                system_response = f"🎉 Congratulation 🎉 Yes, I'm `{self.person}`!"
                end_game = True
            elif user_query == GameWWIA.END_GAME:
                end_game = True
            else:
                end_game, system_response = self.round(
                    user_query=user_query,
                    person=self.person,
                    text=self.text,
                )
            print(">>", system_response)

            # -- Hint
            if (
                self.hint_frequency
                and (not end_game)
                and (self.count_round % self.hint_frequency == 0)
            ):
                print(">>", self.hint._give())

        # End of the game
        print(f"\n >> `{self.person}` and me thanks you for playing 💜 \n")

    def _chat_update(self, message: str, chat_history: list[str]):
        # Init game if needed
        if not self.init:
            self._init_round()

        # Update chat history
        # - Cheat code
        if message == "42":
            system_response = f"I'm `{self.person}` 🦾"
            end_game = False
        # - User stop
        elif message == GameWWIA.END_GAME:
            system_response = f"Thanks you for playing 💜 \n ➡️ Learn more about: [{self.person}](https://en.wikipedia.org/wiki/{self.page_id})"
            message = None
            end_game = True
        elif message == self.person:
            system_response = f"YES ! I'm [{self.person}](https://en.wikipedia.org/wiki/{self.page_id}) 🎉🎉🎉"
            chat_history.append((message, system_response))
            system_response = f"Thanks you for playing 💜 \n ➡️ Learn more about: [{self.person}](https://en.wikipedia.org/wiki/{self.page_id})"
            end_game = True
        # - Normal Round
        else:
            end_game, system_response = self.round(user_query=message)
        chat_history.append((message, system_response))

        # End of the game
        if end_game:
            self.init = False
        # Hint
        elif self.hint_frequency and (self.count_round % self.hint_frequency == 0):
            chat_history.append((None, self.hint._give()))

        return "", chat_history

    def run_gradio(self) -> None:
        """Run the game in a GUI"""
        self.init = False
        with gr.Blocks(theme=gr.themes.Soft()) as demo:
            gr.Markdown("""# 🇼 Which Wiki I Am""")

            chatbot = gr.Chatbot(
                value=[
                    (
                        None,
                        "I'll choose a random person on Wikipedia. You have to guess which person is about asking only closed questions!",
                    )
                ]
            )
            user_query = gr.Textbox(
                label="Enter a closed question:", value="", visible=True
            )
            user_query.submit(
                self._chat_update,
                inputs=[user_query, chatbot],
                outputs=[user_query, chatbot],
            )

            with gr.Row():
                stop = gr.Button("Stop Playing")
                stop_txt = gr.Textbox(value=GameWWIA.END_GAME, visible=False)
                stop.click(
                    self._chat_update,
                    inputs=[stop_txt, chatbot],
                    outputs=[user_query, chatbot],
                )

                clear = gr.ClearButton(
                    components=[user_query, chatbot], value="Clear our dialog"
                )

        demo.launch()
