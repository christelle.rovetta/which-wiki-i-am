from typing import Literal, Callable, Any
from pydantic import BaseModel, Field

from langchain.agents import tool
from langchain_core.utils.function_calling import convert_to_openai_function
from langchain.schema.agent import AgentFinish


def to_llm_functions(l_functions: list[Callable]):
    return [convert_to_openai_function(e) for e in l_functions]


class Answer(BaseModel):
    """Answering to a user question"""

    answer: Literal["yes", "no", "I don't know"] = Field(
        description="Answer to the user, the response must be `yes`, `no` or `I don't know`"
    )


@tool
def victory(user_proposition: str, your_name: str) -> bool:
    """Determine if the user guess your name or not."""
    return {"win": user_proposition == your_name, "supposition": user_proposition}


def route_yn(result) -> dict[str, Any]:
    if isinstance(result, AgentFinish):
        # - Agent doesn't detect a tool
        return {}
    else:
        # - Agent detects the tool `win`
        tools = {"victory": victory}
        if result.tool in tools:
            return tools[result.tool].run(result.tool_input)
        # - Agent detects the tool `Answer`
        if result.tool == "Answer":
            return dict(result.tool_input)
