from abc import ABC, abstractmethod
from typing import Any


class BaseLLMPipeline(ABC):
    @abstractmethod
    def invoke(self, query: str, query_args: dict[str, str] = {}) -> dict[str, Any]:
        """
        Infer using a LLM pipeline
        """
        raise NotImplementedError
