import os
from typing import Any

from langchain.prompts import ChatPromptTemplate
from langchain.agents.output_parsers import OpenAIFunctionsAgentOutputParser
from langchain_openai import ChatOpenAI

from .base_pipeline_lmm import BaseLLMPipeline
from .tools.round import to_llm_functions, Answer, victory, route_yn


class PipelineRoundOpenAI(BaseLLMPipeline):
    def __init__(self) -> None:
        self.pipeline = PipelineRoundOpenAI.get_chain_round()

    @staticmethod
    def get_chain_round():
        prompt = ChatPromptTemplate.from_messages(
            [
                (
                    "system",
                    "You're playing a game with the user. \
                    You've chosen to be `{person}` and the user has to guess who you are by asking you questions. \
                    The user can propose a name for you and you have to decide whether it's a victory or not.\
                    Your answers must be based on the following text: '{text}'.",
                ),
                ("user", "{query}"),
            ]
        )
        openai_functions_yn = to_llm_functions([Answer, victory])
        model = ChatOpenAI(
            temperature=0,
            openai_api_key=os.getenv("OPENAI_API_KEY"),
            model=os.getenv("OPEN_AI_MODEL"),
        ).bind(functions=openai_functions_yn, function_call="auto")

        return prompt | model | OpenAIFunctionsAgentOutputParser() | route_yn

    def invoke(
        self,
        person: str,
        text: str,
        user_query: str,
    ) -> dict[str, Any]:
        return self.pipeline.invoke(
            {"person": person, "text": text, "query": user_query}
        )
