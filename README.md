# Which Wiki I Am

**Which Wiki I Am** is a serious game to discover random person on wikipedia. The game chooses someone and the user have to guess which person is about asking closed questions. (WIP) You can choose to play only with peoples from underrepresented groups. 


This project uses **Generative AI** to response to user questions or gives some hints.


## Play

Rename `sample.env` to `.env` and fill your personal `OpenAI API key`.

* Run docker container in `play` mode:
```
docker compose run --rm play
```


## Dev

Rename `sample.env` to `.env` and fill your personal `OpenAI API key`.

* Run docker container in `dev` mode:
```
docker compose run --rm dev
```

* Test your code:
```
poetry run pytest -s
```

* Play in terminal mode:
```
poetry run python app.py terminal
```

* Play in gui mode:
```
poetry run python app.py gui
```


## References

* [Course - LLM](https://github.com/rasbt/LLMs-from-scratch/tree/main)
* [Course - Functions, Tools and Agents with LangChain](https://www.deeplearning.ai/short-courses/functions-tools-agents-langchain/)
* [Gradio documentation](https://www.gradio.app/docs/gradio/blocks)
* [Quantization](https://towardsdatascience.com/improving-llm-inference-latency-on-cpus-with-model-quantization-28aefb495657)
* [ollama official website](https://ollama.com/)
* [ollama python lib](https://github.com/ollama/ollama-python)


## Next steps

* Use asyncio call in:
    * HintTalkLike

* Add LLM pipelines
    * Add underrepresented option (ollama)

* Fine tuning
    * Fine tunning for classification task yes/no/victory (Mistal ? llama ?)
    * Quantization
    * add model in ollama
    * Compare GPT4 round pipeline with fine tunning