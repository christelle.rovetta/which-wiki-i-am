####################################################################
# Base image
####################################################################
FROM python:3.12 as base

# set the working directory in the container
WORKDIR /opt/workspace

# install dependencies
COPY ./pyproject.toml ./pyproject.toml
RUN python -m pip install --upgrade pip
RUN python -m pip install poetry
RUN poetry config virtualenvs.create false

####################################################################
# Ollama image
####################################################################
FROM python:3.12 as ollama

# Install ollama
RUN curl -fsSL https://ollama.com/install.sh | sh


####################################################################
# Dev image
####################################################################
FROM base as dev

RUN poetry install

####################################################################
# Prod image
####################################################################
FROM base as prod

# copy sources
COPY ./wwia ./wwia

RUN poetry install