import fire

from wwia.game import GameWWIA


class Wwia(object):
    def terminal(self, underrepresented: bool = False):
        from art import tprint

        tprint("Which Wiki I Am?", "random")
        print(
            "\n >> You play a serious game to discover random persons on wikipedia. \
            \n >> I will choose someone and you have to guess which person is about asking only closed questions! \n\n"
        )
        game = GameWWIA(underrepresented=underrepresented)
        game.run_terminal()

    def gui(self, underrepresented: bool = False):
        game = GameWWIA(underrepresented=underrepresented)
        game.run_gradio()


if __name__ == "__main__":
    fire.Fire(Wwia)
