import unittest

from wwia.utils.wikipedia import WikipediaTools
from wwia.utils.text_retrieval import TextRetrieval


class TestTextRetrieval(unittest.TestCase):
    def test_textRetrieval(self):
        big_content = "Taylor_Swift"
        text = WikipediaTools().page_info(page_id=big_content)["text"]

        txt_retrieval = TextRetrieval(k=2)
        txt_retrieval.new_text(text=text)
        txt_context = txt_retrieval.get_context_as_txt("Where is Taylor living ?")
        self.assertTrue(isinstance(txt_context, str))
