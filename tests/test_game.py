import unittest

from wwia.game import GameWWIA


class TestGame(unittest.TestCase):
    def test_round(self):
        game = GameWWIA()
        win = game.round(
            user_query="Are you a man?",
            person="Myléne Farmer",
            text="A French singer, songwriter and actress who has been the best-selling French artist since the 1980s.",
            count=1,
        )
        self.assertFalse(win)
        win = game.round(
            user_query="Are you Myléne Farmer?",
            person="Myléne Farmer",
            text="A French singer, songwriter and actress who has been the best-selling French artist since the 1980s.",
            count=1000,
        )
        self.assertTrue(win)
