import unittest

from wwia.hint.catalog.factory import hint_factory, HintId, HintHungman, HintTalkLike
from wwia.hint.hints import Hints


class TestHint(unittest.TestCase):
    def test_hints(self):
        hints = Hints(
            secret="Alan Turing",
            hints_config={
                HintId.HANGMAN.value: {"difficulty": 3},
                HintId.TALK_LIKE.value: {"num_sentences": 2},
            },
        )
        self.assertEqual(hints.secret, "Alan Turing")

    def test_factory(self):
        hint = hint_factory(
            hint_id=HintId.HANGMAN.value, secret="Olympe de Gouges", difficulty=3
        )
        self.assertEqual(hint.secret, "Olympe de Gouges")

    def test_hungman(self):
        hint = HintHungman(secret="Olympe de Gouges", difficulty=3)
        self.assertEqual(hint._give(), "My name is: `...... .. ......`")
        hint.give()
        self.assertFalse(hint.given)

    def test_talkLike(self):
        hint = HintTalkLike(secret="Frida Khalo")
        self.assertEqual(hint.secret, "Frida Khalo")
