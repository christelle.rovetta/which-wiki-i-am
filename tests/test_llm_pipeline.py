import unittest
from typing import Final

from wwia.pipeline_llm.round_openai import PipelineRoundOpenAI

PERSON: Final[str] = "Myléne Farmer"
TEXT: Final[str] = (
    "Myléne Farmer is a French singer, songwriter and actress who has been the best-selling French artist since the 1980s."
)


class TestLLMTool(unittest.TestCase):
    def test_pipeline_round(self):
        pl = PipelineRoundOpenAI()
        try:
            response = pl.invoke(person=PERSON, text=TEXT, user_query="Are you a man?")
        except Exception as e:
            print("Model error:", e)
        else:
            self.assertIn("answer", response)
            self.assertFalse(response["answer"])
