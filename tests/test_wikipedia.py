import unittest

from wwia.utils.wikipedia import WikipediaTools


class TestWikipediaTool(unittest.TestCase):
    def test_random_page(self):
        page_id = WikipediaTools.random_page_id()
        self.assertTrue(isinstance(page_id, str))

    def test_random_person_page(self):
        _ = WikipediaTools().random_person_page()
        self.assertTrue(True)

    def test_info(self):
        d_info = WikipediaTools().page_info(page_id="Alan_Turing")
        self.assertIn("text", d_info)
